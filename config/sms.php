<?php

return [
    'data' => [
        'format' => ['valid','time','latitude','longitude','current_liters','aquifier_level','uuid']
    ]
];