<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fixmicrocontrollers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('microcontrollers', function (Blueprint $table) {
            $table->string('phone')->after('unique_id')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('microcontrollers', function (Blueprint $table) {
            $table->dropColumn('phone');
        });
    }
}
