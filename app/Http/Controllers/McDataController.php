<?php

namespace imaji\Http\Controllers;

use imaji\McData;
use Illuminate\Http\Request;

class McDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \imaji\McData  $mcData
     * @return \Illuminate\Http\Response
     */
    public function show(McData $mcData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \imaji\McData  $mcData
     * @return \Illuminate\Http\Response
     */
    public function edit(McData $mcData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \imaji\McData  $mcData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, McData $mcData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \imaji\McData  $mcData
     * @return \Illuminate\Http\Response
     */
    public function destroy(McData $mcData)
    {
        //
    }
}
