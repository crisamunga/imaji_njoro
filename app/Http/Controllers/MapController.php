<?php

namespace imaji\Http\Controllers;

use Illuminate\Http\Request;
use imaji\Microcontroller;
use imaji\McData;

class MapController extends Controller
{
    public function index()
    {
        $microcontrollers = Microcontroller::with('mc_datas')->get();

        return view('map.index')->with('microcontrollers', $microcontrollers);
    }
}
