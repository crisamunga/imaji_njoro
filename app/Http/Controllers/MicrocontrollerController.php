<?php

namespace imaji\Http\Controllers;

use imaji\Microcontroller;
use Illuminate\Http\Request;
use Validator;

class MicrocontrollerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mcs = Microcontroller::all();

        return view('admin.microcontrollers.index')->with('microcontrollers', $mcs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.microcontrollers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'unique_id' => 'required|string|max:50|unique:microcontrollers,unique_id',
            'phone' => 'required|regex:/^(\+)?[0-9]{10,12}$/|unique:microcontrollers,phone',
            'name' => 'required|string|max:50',
            'latitude' => 'required|numeric|max:90|min:-90',
            'longitude' => 'required|numeric|max:180|min:-180',
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $input = $request->only('unique_id','name','phone','latitude','longitude');
        $microcontroller = Microcontroller::create($input);
        return redirect()->route('microcontrollers.index', ['flash_message' => 'Microcontroller '.$microcontroller->name.' created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \imaji\Microcontroller  $microcontroller
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('microcontrollers.edit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \imaji\Microcontroller  $microcontroller
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $microcontroller = Microcontroller::findOrFail($id);

        return view('admin.microcontrollers.edit')->with('microcontroller', $microcontroller);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \imaji\Microcontroller  $microcontroller
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $microcontroller = Microcontroller::findOrFail($id);
        
        $validator = Validator::make($request->all(), [
            'unique_id' => 'required|string|max:50|unique:microcontrollers,unique_id,'.$microcontroller->id,
            'phone' => 'required|regex:/^(\+)?[0-9]{10,12}$/|unique:microcontrollers,phone,'.$microcontroller->id,
            'name' => 'required|string|max:50',
            'latitude' => 'required|numeric|max:90|min:-90',
            'longitude' => 'required|numeric|max:180|min:-180',
        ]);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $input = $request->only('unique_id','name','phone','latitude','longitude');
        $input = array_filter($input, 'strlen');
        $microcontroller->fill($input);
        $microcontroller->save();

        return redirect()->route('microcontrollers.index', ['flash_message' => 'Microcontroller '.$microcontroller->name.' created successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \imaji\Microcontroller  $microcontroller
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $microcontroller = Microcontroller::findOrFail($id);
        $microcontroller->delete();
        return redirect()->route('microcontrollers.index', ['flash_message' => 'Microcontroller '.$microcontroller->name.' created successfully']);
    }
}
