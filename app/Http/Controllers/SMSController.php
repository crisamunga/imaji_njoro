<?php

namespace imaji\Http\Controllers;

use Illuminate\Http\Request;
use imaji\Microcontroller;
use imaji\McData;

use imaji\Utilities\SMSUtilities;
use Log;

class SMSController extends Controller
{
    public function receive(Request $request)
    {
        $phone = $request->phone;
        $text = $request->text;

        $mc = Microcontroller::where('phone',$phone)->first();
        
        if ($mc == null) {
            Log::alert('Message received from unknown phone '.$phone);
            return;
        }

        $data = SMSUtilities::decode($text);
        $mc->mc_datas()->create($data);
    }
}
