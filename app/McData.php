<?php

namespace imaji;

use Illuminate\Database\Eloquent\Model;

class McData extends Model
{
    protected $fillable = [
            'valid','time','latitude','longitude','current_liters','consumption','aquifier_level','uuid'
    ];

    public function microcontroller()
    {
        return $this->belongsTo('imaji\Microcontroller','microcontroller_id');
    }
}
