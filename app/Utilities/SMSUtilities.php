<?php

namespace imaji\Utilities;

use Log;
use Carbon\Carbon;

class SMSUtilities
{
    public static function decode($message)
    {
        $protocol = config('sms');
        $data = explode(',',$message);
        if (count($data) != count($protocol['data']['format'])) {
            Log::error('Invalid message format sent');
            Log::error($message);
            return null;
        }
        $decodeddata = array();
        for ($i = 0; $i  < count($data); $i++) {
            $entry = $protocol['data']['format'][$i];
            $parameter = $data[$i];
            if ($entry == 'time') {
                $parameter = Carbon::createFromTimestamp($parameter)."";
            }
            $decodeddata[$entry] = $parameter;
        }
        return $decodeddata;
    }
}