<?php

namespace imaji;

use Illuminate\Database\Eloquent\Model;

class Microcontroller extends Model
{
    use HasUuid;

    public $incrementing = false;

    protected $fillable = [
            'unique_id','name','phone','latitude','longitude'
    ];

    public function getLastDataAttribute()
    {
        return $this->mc_datas->last();
    }

    public function mc_datas() {
        return $this->hasMany('imaji\McData','microcontroller_id');
    }
}
