var infowindow;
var map;
var geocoder;
var markerlist = [];

function initMap() {
    var mapelement = document.getElementById('map');
    var myLatLng = new google.maps.LatLng(-1, 37);
    map = new google.maps.Map(mapelement, {
        center: myLatLng,
        zoom: 8
    });

    infowindow = new google.maps.InfoWindow({
        content: null
    });

    geocoder = new google.maps.Geocoder;

    showMicrocontrollers();
}

function showMicrocontrollers() {
    for (let i = 0; i < microcontrollers.length; i++) {
        placeMarkerFromMicrocontroller(i);
    }
}

function placeMarkerFromMicrocontroller(index)
{
    const microcontroller = microcontrollers[index];
    const data = microcontroller['mc_datas'];
    var latitude = microcontroller['latitude'];
    var longitude = microcontroller['longitude'];
    var aquiferlvl = 'Not measured';
    var watervolume = 'Not measured';
    if (data && data.length > 0) {
        latitude = data[data.length -1]['latitude'];
        longitude = data[data.length -1]['longitude'];
        aquiferlvl = data[data.length -1]['aquifier_level'];
        watervolume = data[data.length -1]['current_liters'];
    }

    var pos = new google.maps.LatLng(latitude, longitude);
    var marker = new google.maps.Marker({
        position: pos,
        map: map,
        title: microcontroller['name'],
        icon: "img/flowmeter.png"
    });
    var iwindow = new google.maps.InfoWindow({
        content: getInfoWindowContent(microcontroller, aquiferlvl, watervolume, latitude, longitude)
    });

    google.maps.event.addListener(marker, 'click', function() {
        iwindow.open(map,marker);
    });
}

function getInfoWindowContent(microcontroller, aquiferlvl, watervolume, latitude, longitude)
{
    return '' +
        '<div class="card">' +
            '<div class="card-title"><h5>' + microcontroller['name'] + '</h5></div>' +
            '<div class="card-body">' +
                '<h6>Location details</h6>' +
                '<div><b>Current location: </b>' + latitude + "," + longitude + '</div>' +
                '<h6>Live data</h6>' +
                '<div><b>Aquifer level: </b>' + aquiferlvl + '</div>' +
                '<div><b>Water level: </b>' + watervolume + '</div>' +
            '</div>' +
        '</div>';
}

google.maps.event.addDomListener(window, 'load', initMap);