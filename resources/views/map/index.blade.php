@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-2 text-center">
        <h3>Active water points</h3>
        <div>
            <table class="table table-hover">
                <thead class="bg-imaji">
                    <tr><th>Microcontroller</th></tr>
                </thead>
                <tfoot class="bg-dark text-light">
                    <tr><th>Microcontroller</th></tr>
                </tfoot>
                <tbody>
                    @foreach ($microcontrollers as $microcontroller)
                        <tr>
                            <td>
                                <div>{{$microcontroller->name}}</div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>                
        </div>
    </div>
    <div class="col-md-10">
        <div id="map" class="container-fluid"></div>
    </div>
</div>
@endsection

@section('footer')
<script>
    var microcontrollers = @json($microcontrollers);
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAH_JEW5yzDXLdpaXVLzx01rtvPu3j_8W4"></script>
<script src="{{ mix('js/map.js')}}"></script>
@endsection