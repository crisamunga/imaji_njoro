@extends('layouts.app')

@section('content')
<div class="text-center">
    <h1>(500) Internal Server Error</h1>
    <p>We're having some trouble but we'll have it fixed ASAP</p>
</div>
@endsection