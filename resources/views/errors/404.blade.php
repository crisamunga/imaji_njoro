@extends('layouts.app')

@section('content')
    <div class="text-center">
        <h1>(404) Page not found</h1>
        <p>The page you requested does not exist on this server</p>
        <h5>Please check your URL</h5>
    </div>
@endsection