@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="container-fluid">
                        <div class="card-deck">
                            <div class="card bg-dark text-light">
                                <div class="card-body text-center">
                                    <div>
                                        <span class="mdi mdi-48px mdi-raspberrypi"></span>
                                    </div>
                                    <p class="card-text">
                                        Add, edit, remove microcontrollers from the system
                                    </p>
                                    <a href="{{route('microcontrollers.index')}}" class="btn btn-yellow">Manage Microcontrollers</a>
                                </div>
                            </div>
                            <div class="card bg-dark text-light">
                                <div class="card-body text-center">
                                    <div>
                                        <span class="mdi mdi-map mdi-48px"></span>
                                    </div>
                                    <p class="card-text">
                                        View microcontrollers and current data on map
                                    </p>
                                    <a href="{{route('map')}}" class="btn btn-yellow">Map</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
