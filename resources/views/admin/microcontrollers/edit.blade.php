@extends('layouts.app')

@section('content')

<div class="container text-center">
    <h1>Add microcontrollers</h1>
    <h4>The fields marked (*) are mandatory</h4>
    @if ($errors)
        <p class="text-danger">It seems there were errors in your input</p>
    @endif
</div>

<div class="container">
    
{{ Form::model($microcontroller,array('route' => ['microcontrollers.update', $microcontroller->id],'method' => 'PUT','enctype' => 'multipart/form-data')) }}

    <div class="form-group row {{ $errors->has('unique_id') ? ' has-error' : '' }}">
        {{ Form::label('unique_id', 'Unique id *', ['class' => 'col-sm-2 col-form-label']) }}<br>
        <div class="col-sm-10">
            {{ Form::text('unique_id', $microcontroller->unique_id, array('class' => 'form-control input-underline', 'maxlength' => '50', 'required', 'placeholder' => 'Type a onique id here')) }}
        </div>
        @if ($errors->has('unique_id'))
            <span class="help-block">
                <strong>{{ $errors->first('unique_id') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group row {{ $errors->has('name') ? ' has-error' : '' }}">
        {{ Form::label('name', 'Name *', ['class' => 'col-sm-2 col-form-label']) }}<br>
        <div class="col-sm-10">
            {{ Form::text('name', $microcontroller->name, array('class' => 'form-control input-underline', 'maxlength' => '50', 'required', 'placeholder' => 'Type a name for the microcontroller here')) }}
        </div>
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group row {{ $errors->has('phone') ? ' has-error' : '' }}">
        {{ Form::label('phone', 'Phone *', ['class' => 'col-sm-2 col-form-label']) }}<br>
        <div class="col-sm-10">
            {{ Form::text('phone', $microcontroller->phone, array('class' => 'form-control input-underline', 'maxlength' => '50', 'required', 'placeholder' => 'Type a phone number here e.g 254700000000')) }}
        </div>
        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group row {{ $errors->has('latitude') ? ' has-error' : '' }}">
        {{ Form::label('latitude', 'Latitude *', ['class' => 'col-sm-2 col-form-label']) }}<br>
        <div class="col-sm-10">
            {{ Form::number('latitude', $microcontroller->latitude, array('class' => 'form-control input-underline', 'required', 'step' => '0.000001', 'min' => '-90', 'max' => '90','placeholder' => 'Type a latitude for the microcontroller here')) }}
        </div>
        @if ($errors->has('latitude'))
            <span class="help-block">
                <strong>{{ $errors->first('latitude')}}</strong>
            </span>
        @endif
    </div>

    <div class="form-group row {{ $errors->has('longitude') ? ' has-error' : '' }}">
        {{ Form::label('longitude', 'Longitude *', ['class' => 'col-sm-2 col-form-label']) }}<br>
        <div class="col-sm-10">
            {{ Form::number('longitude', $microcontroller->longitude, array('class' => 'form-control input-underline', 'required', 'step' => '0.000001', 'min' => '-180', 'max' => '180', 'placeholder' => 'Type a longitude for the microcontroller here')) }}
        </div>
        @if ($errors->has('longitude'))
            <span class="help-block">
                <strong>{{ $errors->first('longitude') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <a href="{{ route('microcontrollers.index') }}" class="btn btn-danger">
            <i class="mdi mdi-arrow-left mdi-18px"></i> Go back
        </a>

        <button type="submit" class="btn btn-primary btn-submit">
            <i class="mdi mdi-upload mdi-18px"></i> Update microcontroller
        </button>
    </div>

{{ Form::close() }}

</div>

@endsection