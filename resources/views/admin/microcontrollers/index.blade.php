@extends('layouts.app')

@section('content')

<div class="container-fluid text-center">
    <h1>Manage microcontrollers</h1>
    <h4>Add, edit or remove microcontrollers from the system</h4>
</div>

<div class="container-fluid">
    <table class="table table-bordered table-hover">
        <thead class="bg-imaji">
            <tr>
                <th>Unique id</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>Operations</th>
            </tr>
        </thead>
        <tfoot class="bg-dark text-light">
            <tr>
                <th>Unique id</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>Operations</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach ($microcontrollers as $microcontroller)
                <tr>
                    <td>{{$microcontroller->unique_id}}</td>
                    <td>{{$microcontroller->name}}</td>
                    <td>{{$microcontroller->phone}}</td>
                    <td>{{$microcontroller->latitude}}</td>
                    <td>{{$microcontroller->longitude}}</td>
                    <td>
                        <a href="{{route('microcontrollers.edit',$microcontroller->id)}}" class="btn btn-info"><span class="mdi mdi-plus"></span> Edit</a>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['microcontrollers.destroy', $microcontroller->id] ]) !!}
                            <button type="submit" class="btn btn-danger">
                                <i class="mdi mdi-delete mdi-18px"></i>
                                Remove
                            </button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="container">
    <a href="{{route('microcontrollers.create')}}" class="btn btn-info"><span class="mdi mdi-plus"></span> Add microcontroller</a>
</div>
@endsection